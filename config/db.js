const mongoose = require('mongoose');
const logging = require('../logs/log');

const connectDB = async () => {
  try {
    const conn = await mongoose.connect(process.env.MONGO_URI, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    });

    console.log(`MongoDb Connected ${conn.connection.host}`);
  } catch (err) {
    logging('Error', error.message);
    console.error(`Error: ${err}`);
    process.exit();
  }
};

module.exports = connectDB;
