const asyncHandler = require('express-async-handler');
const User = require('../models/userModel');
const logging = require('../logs/log');
const Truck = require('../models/truckModel');

const getProfileInfo = asyncHandler(async (req, res)=> {
  try {
    const myUser = await User.findById(req.user._id);
    if (myUser) {
      logging('Info', 'Information about the user has been sent');
      res.status(200).json({
        user: {
          _id: req.user._id,
          role: myUser.role,
          email: myUser.email,
          createdDate: myUser.createdDate,
        },
      });
    } else {
      logging('Info', 'User is not found.');
      res.status(400).json({
        message: 'No such user',
      });
    }
  } catch (e) {
    logging('Error', 'Error during getting user account');
    res.status(500).json({
      message: 'Something went wrong. Try restarting',
    });
  }
});
const deleteUser = asyncHandler(async (req, res) => {
  try {
    const userId = req.user._id;
    const onLoadTruck = await Truck.findOne(
        {createdBy: userId, status: 'OL'},
    );
    if (onLoadTruck) {
      logging('Error', 'Forbidden to delete account while driver is on load.');
      return res.status(400).json({
        message: 'Forbidden to delete account while driver is on load.',
      });
    }
    await User.findByIdAndDelete(userId);
    await Truck.deleteMany({createdBy: userId});
    logging('Info', 'Profile deleted successfully');
    res.status(200).json({message: 'Profile deleted successfully'});
  } catch (e) {
    logging('Error', 'Error during deleting user account');
    res.status(500).json({message: 'Something went wrong. Try restarting'});
  }
});
const changeProfilePassword = asyncHandler(async (req, res)=> {
  try {
    const userId = req.user._id;
    const onLoadTruck = await Truck.findOne({
      createdBy: userId,
      status: 'OL',
    } );
    if (onLoadTruck) {
      logging('Error', 'Forbidden to update account while driver is on load.');
      return res.status(400).json({
        message: 'Forbidden to update account while driver is on load.',
      });
    }
    const myUser = await User.findById(req.user._id);
    const newPassword = req.body.newPassword;
    if (myUser) {
      myUser.password = newPassword;
      await myUser.save();
      logging('Info', 'Information about the user has been updated');
      res.status(200).json({
        message: 'Password changed successfully',
      });
    } else {
      res.status(400).json({
        message: 'Failed to update password!',
      });
    }
  } catch (e) {
    logging('Error', 'Error during updating user account');
    res.status(500).json({
      status: 'Something went wrong. Try restarting',
    });
  }
});
module.exports = {getProfileInfo, deleteUser, changeProfilePassword};
