const express = require('express');
const dotenv = require('dotenv');
const connectDB = require('./config/db');
const authRoutes = require('./routes/AuthRoutes');
const profileRoutes = require('./routes/ProfileRoutes');
const trucksRoutes = require('./routes/TrucksRoutes');
const loadsRoutes = require('./routes/LoadsRoutes');
const app = express();
dotenv.config();
connectDB();


app.use(express.json());
app.get('/', (req, res)=> {
  res.send('API is running');
});
app.use('/api/auth', authRoutes);
app.use('/api/users', profileRoutes);
app.use('/api/trucks', trucksRoutes);
app.use('/api/loads', loadsRoutes);
const PORT = process.env.PORT || 8080;
app.listen(PORT, console.log(`Server started on Port ${PORT}`));
