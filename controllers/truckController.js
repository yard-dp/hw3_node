const asyncHandler = require('express-async-handler');
const Truck = require('../models/truckModel');
const User = require('../models/userModel');
const logging = require('../logs/log');
const sizeOfTruck = require('./helpers/truck_types');


const getTrucks = asyncHandler(async (req, res)=> {
  try {
    const driverId = req.user._id;
    const user = await User.findById(driverId);
    if (user.role !== 'driver') {
      logging('Info', 'User is not a driver');
      return res.status(400).json({message: 'The user is not a driver'});
    }
    const trucks = await Truck.find({
      createdBy: driverId,
    });
    if (!trucks.length) {
      logging('Info', 'No trucks for this driver');
      return res.status(400).json({message: 'No trucks for this driver'});
    } else {
      logging('Info', 'List of trucks has been sent to the driver.');
      return res.status(200).json({trucks: trucks});
    }
  } catch (e) {
    logging('Error', `Loads have not been sent, ${e}`);
    res.status(500).json({
      status: 'Something went wrong. Try restarting',
    });
  }
});
const postTrucks = asyncHandler(async (req, res)=> {
  try {
    const driverId = req.user._id;
    const user = await User.findById(driverId);
    if (user.role != 'driver') {
      logging('Info', 'User is not a driver');
      return res.status(400).json({status: 'The user is not a driver'});
    }
    const {type} = req.body;
    const [width, length, height, payload] = sizeOfTruck(type);
    const count = await Truck.countDocuments({driverId});
    if (count >= 50) {
      logging('Info', 'Number of user\'s trucks is more than 50');
      return res.status(400).json({
        message: 'Limit exceeded (50). You cannot add no more trucks.',
      });
    }
    const truck = new Truck({
      createdBy: driverId,
      type,
      width,
      length,
      height,
      payload,
    });
    await truck.save();
    logging('Info', 'A new truck has been created');
    res.status(200).json({
      message: 'Truck created successfully',
    });
  } catch (e) {
    logging('Error', `Truck hasn't been added, ${e}`);
    res.status(500).json({
      message: 'Something went wrong. Try restarting',
    });
  }
});
const assignTruckById = asyncHandler(async (req, res)=> {
  try {
    const truckId = req.params.id;
    const driverId = req.user._id;
    const user = await User.findById(driverId);
    if (user.role != 'driver') {
      logging('Info', 'User is not a driver');
      return res.status(400).json({status: 'The user is not a driver'});
    }

    const toAssignTruck = await Truck.findById(truckId);
    if (String(toAssignTruck.createdBy) !== String(driverId)) {
      logging('Info', 'Access denied.');
      return res.status(400).json({
        message: 'Access denied.',
      });
    }

    await Truck.findOneAndUpdate({assignedTo: driverId}, {assignedTo: null});
    await Truck.findByIdAndUpdate(
        truckId, {
          assignedTo: driverId,
        },
    );

    logging('Info', 'Truck assigned.');
    return res.status(200).json({
      message: 'Truck assigned successfully',
    });
  } catch (e) {
    logging('Error', `Truck hasn't been assigned, ${e}`);
    res.status(500).json({
      message: 'Something went wrong. Try restarting',
    });
  }
});

const deleteTruckById = asyncHandler(async (req, res)=> {
  try {
    const truckId = req.params.id;
    const driverId = req.user._id;
    const user = await User.findById(driverId);
    if (user.role != 'driver') {
      logging('Info', 'User is not a driver');
      return res.status(400).json({status: 'The user is not a driver'});
    }
    const toDeleteTruck = await Truck.findById(truckId);
    if (String(toDeleteTruck.createdBy) !== String(driverId)) {
      logging('Info', 'Access denied.');
      return res.status(400).json({
        message: 'Access denied.',
      });
    }

    const onLoadTruck = await Truck.findOne({
      createdBy: driverId,
      status: 'OL',
    } );

    const assignedTruck = await Truck.findOne({
      _id: truckId,
      assignedTo: driverId,
    } );

    if (onLoadTruck || assignedTruck) {
      logging('Error', 'Truck is assigned or driver is on load');
      return res.status(400).json({
        message: 'Forbidden to delete truck.',
      });
    } else {
      await Truck.findByIdAndDelete(truckId);

      logging('Info', 'Truck has been deleted.');
      return res.status(200).json({
        message: 'Truck deleted successfully',
      });
    }
  } catch (e) {
    logging('Error', `Truck hasn't been deleted, ${e}`);
    res.status(500).json({
      message: 'Something went wrong. Try restarting',
    });
  }
});
const getTruckById = asyncHandler(async (req, res)=> {
  try {
    const truckId = req.params.id;
    const driverId = req.user._id;
    console.log(truckId);
    console.log(driverId);
    const user = await User.findById(driverId);
    if (user.role != 'driver') {
      logging('Info', 'User is not a driver');
      return res.status(400).json({status: 'The user is not a driver'});
    }
    const truck = await Truck.findById(truckId);
    if (truck) {
      logging('Info', 'Truck has been found.');
      return res.status(200).json({truck:
        {_id: truckId, createdBy: truck.createdBy,
          assignedTo: truck.assignedTo, type: truck.type, status: truck.status,
          createdDate: truck.createdDate}});
    } else {
      logging('Error', 'Truck has not been found');
      res.status(400).json({message: 'Truck has not been found'});
    }
  } catch (e) {
    logging('Error', `Truck hasn't been found, ${e}`);
    res.status(500).json({
      message: 'Something went wrong. Try restarting',
    });
  }
});
const updTruckById = asyncHandler(async (req, res)=> {
  try {
    const truckId = req.params.id;
    const driverId = req.user._id;
    const {type} = req.body;
    const user = await User.findById(driverId);
    if (user.role != 'driver') {
      logging('Info', 'User is not a driver');
      return res.status(400).json({status: 'The user is not a driver'});
    }
    const truck = await Truck.findById(truckId);
    if (truck) {
      logging('Info', 'Truck details changed successfully');
      truck.type = type;
      await truck.save();
      res.status(200).json({message: 'Truck details changed successfully'});
    } else {
      logging('Error', 'Truck details did not change');
      res.status(400).json({message: 'Truck details did not change'});
    }
  } catch (e) {
    logging('Error', `Truck details did not change`);
    res.status(500).json({
      message: 'Something went wrong. Try restarting',
    });
  }
});
module.exports = {getTrucks, postTrucks, assignTruckById,
  deleteTruckById, getTruckById, updTruckById};
