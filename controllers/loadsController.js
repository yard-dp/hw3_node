const asyncHandler = require('express-async-handler');
const Load = require('../models/loadModel');
const User = require('../models/userModel');
const Truck = require('../models/truckModel');
const logging = require('../logs/log');
const findTruck = require('../middlewares/loadsMiddleware');

const getLoads = asyncHandler(async (req, res)=> {
  try {
    const userId = req.user._id;
    let loads;

    const user = await User.findById(userId);

    if (!user) {
      logging('Info', 'User is not found.');
      return res.status(400).json({
        message: 'User is not found.',
      });
    }

    if (user.role === 'driver') {
      loads = await Load.find({
        assignedTo: userId,
        status: 'ASSIGNED',
      });
    } else {
      loads = await Load.find({
        createdBy: userId,
      });
    }

    if (!loads.length) {
      logging('Info', 'List of loads is empty.');
      return res.status(400).json({
        message: 'You do not have any loads yet.',
      });
    }

    const outputLoads = loads.map((load) => {
      return {
        '_id': load._id,
        'assigned_to': load.assignedTo || 'Not assigned',
        'created_by': load.createdBy,
        'status': load.status,
        'state': load.state,
        'logs': load.logs,
        'dimensions': {
          'width': load.dimensions.width,
          'length': load.dimensions.length,
          'height': load.dimensions.height,
        },
        'payload': load.payload,
        'created_date': load.createdAt,
        'pickup_address': load.pickupAddress,
        'delivery_address': load.deliveryAddress,
      };
    });

    logging('Info', 'List of loads has been sent to the shipper.');
    res.status(200).json({
      'loads': outputLoads,
    });
  } catch (e) {
    logging('Error', `Loads have not been sent, ${e}`);
    res.status(500).json({
      status: 'Something went wrong. Try restarting',
    });
  }
});

const postLoads = asyncHandler(async (req, res)=> {
  try {
    const shipperId = req.user._id;

    const user = await User.findById(shipperId);

    if (!user) {
      logging('Info', 'User is not found.');
      return res.status(400).json({
        message: 'User is not found.',
      });
    }

    if (user.role != 'shipper') {
      logging('Info', 'User is not a shipper');
      return res.status(400).json({
        message: 'User is not a shipper',
      });
    }

    const {name, dimensions, payload,
      // eslint-disable-next-line camelcase
      pickup_address, delivery_address} = req.body;
    const logs = [{
      message: 'Load created',
      time: Date.now(),
    }];

    const load = new Load({
      name,
      createdBy: shipperId,
      dimensions,
      payload,
      // eslint-disable-next-line camelcase
      pickup_address,
      // eslint-disable-next-line camelcase
      delivery_address,
      logs,
    });

    await load.save();

    logging('Info', 'Load created successfully');
    res.status(200).json({
      message: 'Load created successfully',
    });
  } catch (e) {
    logging('Error', `Load hasn't been added, ${e}`);
    res.status(500).json({
      status: 'Something went wrong. Try restarting',
    });
  }
});

const getLoadById = asyncHandler(async (req, res)=> {
  try {
    const loadId = req.params.id;
    const userId = req.user._id;

    const load = await Load.findById(loadId);

    if (!load) {
      logging('Error', 'The load does not exist');
      return res.status(400).json({
        message: 'The load does not exist',
      });
    }
    if (String(load.createdBy) !== String(userId)) {
      logging('Info', 'Access denied');
      return res.status(400).json({
        message: 'Access denied',
      });
    }

    const shipment = {
      '_id': load._id,
      'status': load.status,
      'state': load.state,
      'created_by': load.createdBy,
      'name': load.name,
      'dimensions': {
        'width': load.dimensions.width,
        'length': load.dimensions.length,
        'height': load.dimensions.height,
      },
      'payload': load.payload,
      'created_date': load.createdAt,
      'logs': load.logs,
      'pickup_address': load.pickupAddress,
      'delivery_address': load.deliveryAddress,
    };

    if (load.assignedTo) {
      shipment['assigned_to'] = load.assignedTo;
    } else {
      shipment['assigned_to'] = 'Not assigned';
    }

    logging('Info', 'Info about the load has been sent');
    return res.status(200).json({
      load: shipment,
    });
  } catch (e) {
    logging('Error', `Shipment info has not been sent, ${e}`);
    res.status(500).json({
      message: 'Something went wrong. Try restarting',
    });
  }
});
const updLoadById = asyncHandler(async (req, res)=> {
  try {
    const loadId = req.params.id;
    const shipperId = req.user._id;

    const previousLoad = await Load.findById(loadId);

    if (!previousLoad) {
      logging('Info', 'This load does not exist');
      return res.status(400).json({
        message: 'This load does not exist',
      });
    }

    if (String(previousLoad.createdBy) !== String(shipperId)) {
      logging('Info', 'Access denied');
      return res.status(400).json({
        message: 'Access denied',
      });
    }

    if (previousLoad.status !== 'NEW') {
      logging(
          'Info',
          'Cannot update info about the load. Its status is not new.',
      );
      return res.status(400).json({
        message: 'Cannot update info about the load. Its status is not new.',
      });
    }

    const update = {
      dimensions: {
        width: req.body.dimensions.width ||
                    previousLoad.dimensions.width,
        length: req.body.dimensions.length ||
                    previousLoad.dimensions.length,
        height: req.body.dimensions.height ||
                    previousLoad.dimensions.height,
      },
      state: req.body.state || previousLoad.state,
      name: req.body.name || previousLoad.name,
      payload: req.body.payload || previousLoad.payload,
      pickupAddress: req.body.pickupAddress ||
                previousLoad.pickupAddress,
      deliveryAddress: req.body.deliveryAddress ||
                previousLoad.deliveryAddress,
    };

    await Load.findByIdAndUpdate(loadId, update);

    logging('Info', 'Info about the load is updated.');
    res.status(200).json({
      message: 'Load information updated successfully.',
    });
  } catch (e) {
    logging('Error', `Load has not been updated, ${e}`);
    res.status(500).json({
      message: 'Something went wrong. Try restarting',
    });
  }
});

const postLoadById = asyncHandler(async (req, res)=> {
  try {
    const loadId = req.params.id;
    const shipperId = req.user._id;

    const previousLoad = await Load.findById(loadId);

    if (!previousLoad) {
      logging('Info', 'This load does not exist');
      return res.status(400).json({message: 'This load does not exist'});
    }

    if (String(previousLoad.createdBy) !== String(shipperId)) {
      logging('Info', 'Access denied');
      return res.status(400).json({message: 'Access denied'});
    }

    if (previousLoad.status !== 'NEW') {
      logging(
          'Info',
          'Cannot post the load because its status is not new.',
      );
      return res.status(400).json({
        message: 'Cannot post the load. Its status is not new.',
      });
    }

    await Load.findByIdAndUpdate(loadId, {status: 'POSTED'});
    logging('Info', 'The load has been posted.');

    const truckState = findTruck(loadId);

    truckState.then((truck) => {
      if (truck.state === 'No truck') {
        return res.status(200).json({
          message: 'No drivers found',
        });
      } else if (truck.state === 'En route') {
        return res.status(200).json({
          message: 'Load posted successfully',
          driver_found: true,
        });
      } else {
        return res.status(400).json({
          message: 'Something went wrong. Try restarting.',
        });
      }
    });
  } catch (e) {
    logging('Error', `Load has not been posted, ${e}`);
    res.status(500).json({
      status: 'Something went wrong. Try restarting',
    });
  }
});
const deleteLoadById = asyncHandler(async (req, res)=> {
  try {
    const loadId = req.params.id;
    const shipperId = req.user._id;

    const previousLoad = await Load.findById(loadId);

    if (!previousLoad) {
      logging('Info', 'This load does not exist');
      return res.status(400).json({
        message: 'This load does not exist',
      });
    }

    if (String(previousLoad.createdBy) !== String(shipperId)) {
      logging('Info', 'Access denied');
      return res.status(400).json({
        message: 'Access denied',
      });
    }

    if (previousLoad.status !== 'NEW') {
      logging(
          'Info',
          'Cannot delete the load because its status in not new',
      );
      return res.status(400).json({
        message: 'Cannot delete the load. Its status is not new.',
      });
    }

    await Load.findByIdAndDelete(loadId);

    logging('Info', 'Load has been deleted.');
    return res.status(200).json({
      message: 'Load has been successfully deleted.',
    });
  } catch (e) {
    logging('Error', `Load has not been deleted, ${e}`);
    return res.status(500).json({
      message: 'Something went wrong. Try restarting',
    });
  }
});
const getUsersActiveLoad = asyncHandler(async (req, res)=> {
  try {
    const driverId = req.user._id;
    const user = await User.findById(driverId);

    if (!user) {
      logging('Info', 'User is not found.');
      return res.status(400).json({
        message: 'User is not found.',
      });
    }

    if (user.role != 'driver') {
      logging('Info', 'User is not a driver');
      return res.status(400).json({
        message: 'User is not a driver',
      });
    }

    const load = await Load.findOne({assignedTo: driverId, status: 'ASSIGNED'});
    if (load) {
      if (load.state !== 'Ready to Pick Up' &&
      load.state !== 'Arrived to Delivery') {
        logging('Info', 'Found active Load');
        return res.status(200).json({
          load: load,
        });
      } else {
        logging('Error', 'Found no active Loads');
        return res.status(400).json({message: 'Found no active Loads'});
      }
    } else {
      logging('Error', 'Found no Loads');
      return res.status(400).json({message: 'Found no Loads'});
    }
  } catch (e) {
    logging('Error', `Load hasn't been found, ${e}`);
    return res.status(500).json({
      message: 'Something went wrong. Try restarting',
    });
  }
});
const patchActiveLoadState = asyncHandler(async (req, res)=> {
  try {
    const driverId = req.user._id;
    const user = await User.findById(driverId);

    if (!user) {
      logging('Info', 'User is not found.');
      return res.status(400).json({
        message: 'User is not found.',
      });
    }

    if (user.role != 'driver') {
      logging('Info', 'User is not a driver');
      return res.status(400).json({
        message: 'User is not a driver',
      });
    }

    const load = await Load.findOne({assignedTo: driverId, status: 'ASSIGNED'});
    let newState;
    if (load) {
      if (load.state !== 'Ready to Pick Up' ||
      load.state !== 'Arrived to Delivery') {
        logging('Info', 'Found active Load');
      } else {
        logging('Error', 'Found no active Loads');
        return res.status(400).json({message: 'Found no active Loads'});
      }
      if (load.status !== 'ASSIGNED') {
        logging('Info', 'Forbidden to change state');
        return res.status(400).json({
          message: 'Forbidden to change state',
        });
      }
    } else {
      logging('Error', 'Found no Loads');
      return res.status(400).json({message: 'Found no Loads'});
    }
    console.log(load.state);
    switch (load.state) {
      case 'En route to Pick Up':
        newState = 'Arrived to Pick Up';
        break;
      case 'Arrived to Pick Up':
        newState = 'En route to Delivery';
        break;
      case 'En route to Delivery':
        newState = 'Arrived to Delivery';
        break;
      default:
        newState = null;
        break;
    }

    const update = {
      state: newState,
      $push: {
        logs: {
          message: `Current state of load is ${newState}`,
          time: Date.now(),
        },
      },
    };
    console.log(update);
    if (newState === 'Arrived to Delivery') {
      update['status'] = 'SHIPPED';

      await Truck.findOneAndUpdate({
        createdBy: driverId,
      }, {
        status: 'IS',
      } );
    }

    await Load.findByIdAndUpdate(
        load._id,
        update,
    );

    logging('Info', 'Load status changed successfully');
    return res.status(200).json({
      message: 'Load status changed successfully',
    });
  } catch (e) {
    logging('Error', `Load hasn't been found, ${e}`);
    return res.status(500).json({
      message: 'Something went wrong. Try restarting',
    });
  }
});
const getShippingInfo = asyncHandler(async (req, res)=> {
  try {
    const loadId = req.params.id;
    const shipperId = req.user._id;
    const user = await User.findById(shipperId);
    if (!user) {
      logging('Info', 'User is not found.');
      return res.status(400).json({
        message: 'User is not found.',
      });
    }

    if (user.role != 'shipper') {
      logging('Info', 'User is not a driver');
      return res.status(400).json({
        message: 'User is not a driver',
      });
    }

    const load = await Load.findById(loadId);
    if (load) {
      if (load.state !== 'Ready to Pick Up' &&
      load.state !== 'Arrived to Delivery') {
        logging('Info', 'Found active Load');
        const truck = await Truck.find({assignedTo: load.assignedTo});
        res.status(200).json({load: load, truck: truck[0]});
      } else {
        logging('Error', 'Found no active Loads');
        res.status(400).json({message: 'Found no active Loads'});
      }
    } else {
      logging('Error', 'Found no Loads');
      res.status(400).json({message: 'Found no Loads'});
    }
  } catch (e) {
    logging('Error', `Load hasn't been found, ${e}`);
    res.status(500).json({
      message: 'Something went wrong. Try restarting',
    });
  }
});
module.exports = {getLoads, postLoads, getLoadById, updLoadById,
  postLoadById, deleteLoadById, getUsersActiveLoad,
  patchActiveLoadState, getShippingInfo};
