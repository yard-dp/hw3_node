const express = require('express');
const {getTrucks, postTrucks, assignTruckById,
  deleteTruckById, getTruckById,
  updTruckById} = require('../controllers/truckController');
const {protect} = require('../middlewares/authMiddleware');
const schemas = require('../validation/truckSchemaValid');
const validator = require('../middlewares/schemaMiddleware');
const router = express.Router();

router.get('/', protect, getTrucks);
router.post('/', protect, validator(schemas.createForm, 'body'), postTrucks);
router.post('/:id/assign', protect,
    validator(schemas.paramsCheckForm, 'params'), assignTruckById);
router.delete('/:id', protect,
    validator(schemas.paramsCheckForm, 'params'), deleteTruckById);
router.get('/:id', protect,
    validator(schemas.paramsCheckForm, 'params'), getTruckById);
router.put('/:id', protect, validator(schemas.updateForm, 'body'),
    validator(schemas.paramsCheckForm, 'params'), updTruckById);
module.exports = router;
