const asyncHandler = require('express-async-handler');
const User = require('../models/userModel');
const generateToken = require('../utils/generateToken');
const logging = require('../logs/log');

const registerUser = asyncHandler(async (req, res)=> {
  try {
    const {email, password, role} = req.body;
    const userExists = await User.findOne({email});
    if (userExists) {
      logging(
          'Error',
          'User entered existing email during the registration',
      );
      return res.status(400).json({
        message: 'The email you have entered is already registered',
      });
    }
    const user = await User.create({
      email,
      password,
      role: role.toLowerCase(),
    });
    if (user) {
      res.status(200).json({message: 'Profile created successfully'});
      logging('Info', 'A new user has been created');
    } else {
      res.status(500).json({message: 'Server Error'});
    }
  } catch (e) {
    logging('Error', `User has not registered, ${e}`);
    res.status(500).json({
      status: 'Something went wrong. Try restarting',
    });
  }
});
const loginUser = asyncHandler(async (req, res) => {
  try {
    const {email, password} = req.body;

    const user = await User.findOne({email});

    if (user && (await user.matchPassword(password))) {
      logging('Info', `Token to user has been sent`);
      res.status(200).json({jwt_token: generateToken(user._id)});
    } else {
      logging('Error', 'User has not been authenticated');
      res.status(400).json({message: 'Invalid email or password'});
    }
  } catch (e) {
    logging('Error', 'User has not been authenticated');
    res.status(500).json({
      message: 'Something went wrong. Try restarting',
    });
  }
});
module.exports = {registerUser, loginUser};
