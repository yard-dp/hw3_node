const Joi = require('joi');

module.exports = {
  createForm: Joi.object({
    type: Joi.string()
        .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
        .required(),
  }),

  paramsCheckForm: Joi.object({
    id: Joi.string()
        .regex(/^[0-9a-fA-F]{24}$/),
  }),

  updateForm: Joi.object({
    type: Joi.string()
        .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
        .required(),
  }),
};
