const Joi = require('joi');

module.exports = {
  createForm: Joi.object({
    dimensions: Joi.object({
      width: Joi.number().max(1000).min(0).required(),
      length: Joi.number().max(1000).min(0).required(),
      height: Joi.number().max(1000).min(0).required(),
    }),
    payload: Joi.number().max(5000).min(0).required(),
    pickup_address: Joi.string(),
    delivery_address: Joi.string(),
    name: Joi.string(),
  }),

  paramsCheckForm: Joi.object({
    id: Joi.string()
        .regex(/^[0-9a-fA-F]{24}$/)
        .required(),
  }),

  updateForm: Joi.object({
    dimensions: Joi.object({
      width: Joi.number().max(1000).min(0),
      length: Joi.number().max(1000).min(0),
      height: Joi.number().max(1000).min(0),
    }),
    state: Joi.string(),
    payload: Joi.number().max(5000).min(0),
    pickup_address: Joi.string(),
    delivery_address: Joi.string(),
    name: Joi.string(),
  }),
};
