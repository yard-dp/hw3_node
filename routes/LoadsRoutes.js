const express = require('express');
const {getLoads, getLoadById, postLoads, updLoadById,
  deleteLoadById, getUsersActiveLoad,
  patchActiveLoadState,
  postLoadById, getShippingInfo} = require('../controllers/loadsController');
const {protect} = require('../middlewares/authMiddleware');
const validator = require('../middlewares/schemaMiddleware');
const schemas = require('../validation/loadSchemaValid');

const router = express.Router();
router.get('/', protect, getLoads);
router.post('/', protect, validator(schemas.createForm, 'body'), postLoads);
router.route('/active').get(protect, getUsersActiveLoad);
router.patch('/active/state', protect, patchActiveLoadState);
router.route('/:id').get(protect,
    validator(schemas.paramsCheckForm, 'params'), getLoadById)
    .put(protect, validator(schemas.updateForm, 'body'),
        validator(schemas.paramsCheckForm, 'params'), updLoadById)
    .delete(protect, deleteLoadById);
router.post('/:id/post', protect,
    validator(schemas.paramsCheckForm, 'params'), postLoadById);
router.get('/:id/shipping_info', protect,
    validator(schemas.paramsCheckForm, 'params'), getShippingInfo);
module.exports = router;
